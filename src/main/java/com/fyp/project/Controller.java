package com.fyp.project;

import com.fyp.project.entity.MatricRequest;
import com.fyp.project.entity.Student;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping("fyp")
public class Controller {
    @Autowired
    private StudentService service;
    private static final Logger logger = LoggerFactory.getLogger(Controller.class);
    @PostMapping(value = "/upload",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<List<Student>>> getFileInfo(@RequestParam("file") MultipartFile file,
                                         @RequestParam("numberOfGroups") int numberOfGroups) throws IOException{
        return ResponseEntity.ok(service.uploadFile(file, numberOfGroups));
    }
    @PostMapping("/download")
    public ResponseEntity<?> downloadExcel(@RequestParam("file") MultipartFile file,
                                           @RequestParam("numberOfGroups") int numberOfGroups)throws IOException{
        try {
            ByteArrayInputStream excelFile = service.generateExcel(file, numberOfGroups);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "attachment; filename=students.xlsx");
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                    .body(new InputStreamResource(excelFile));
        }catch (Exception e) {
            logger.error("Failed to process the file.", e);
            return ResponseEntity.badRequest().body("Failed to process the file.");
        }
    }
    @GetMapping("view")
    public List<Student> viewStudents(){
        return service.viewStudents();
    }
    @PostMapping("/add")
    public Student addStudent(@RequestBody Student student){
        return service.addStudent(student);
        //return ResponseEntity.ok("");
    }
    @GetMapping("delete")
    public void deleteStudents(){
        service.deleteStudents();
    }
    @GetMapping("find")
    public Optional<Student> findByMatric_no(@RequestBody MatricRequest matricNo){
        return service.findByMatric_no(matricNo);
    }
}
