package com.fyp.project;

import com.fyp.project.entity.*;
import com.fyp.project.repository.PersonalityRepository;
import com.fyp.project.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.ToDoubleFunction;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;
    private final FeatureNormalizer normalizer;
    private final PersonalityRepository personalityRepository;
    private final RestTemplate restTemplate;


    public List<List<Student>> createBalancedGroups(List<Student> students, int numberOfGroups) {
        normalizer.findMinMaxValues(students);
        normalizer.normalizeStudents(students);
        students.sort(Comparator.comparingDouble(Student::getCgpa));
        List<Double> cgpa = new ArrayList<>();
        for (Student student : students){
            cgpa.add(student.getPersonality().getOpenness());
        }
        System.out.println("avgCgpa = "+avg(cgpa));
        List<List<Student>> groups = new ArrayList<>();
        for (int i = 0; i < numberOfGroups; i++) {
            groups.add(new ArrayList<>());
        }
        int groupIndex = 0;
        boolean reverse = false;
        for (Student student : students) {
            groups.get(groupIndex).add(student);
            if (!reverse) {
                groupIndex++;
                if (groupIndex == numberOfGroups) {
                    groupIndex--;
                    reverse = true;
                }
            } else {
                groupIndex--;
                if (groupIndex < 0) {
                    groupIndex++;
                    reverse = false;
                }
            }
        }
        balanceGroupTraits(groups);
        System.out.println(avgStudentGpa(groups));
        return groups;
    }
    private void balanceGroupTraits(List<List<Student>> groups) {
        double avgOpenness = calculateGlobalAverage(groups,student -> student.getPersonality().getOpenness());
        double avgConscientiousness = calculateGlobalAverage(groups,student -> student.getPersonality().getConscientiousness());
        double avgExtraversion = calculateGlobalAverage(groups,student -> student.getPersonality().getExtraversion());
        double avgAgreeableness = calculateGlobalAverage(groups,student -> student.getPersonality().getAgreeableness());
        double avgNeuroticism = calculateGlobalAverage(groups,student -> student.getPersonality().getNeuroticism());
        swapStudentsBetweenGroups(groups, avgOpenness, student -> student.getPersonality().getOpenness());
        swapStudentsBetweenGroups(groups, avgConscientiousness, student -> student.getPersonality().getConscientiousness());
        swapStudentsBetweenGroups(groups, avgExtraversion, student -> student.getPersonality().getExtraversion());
        swapStudentsBetweenGroups(groups, avgAgreeableness, student -> student.getPersonality().getAgreeableness());
        swapStudentsBetweenGroups(groups, avgNeuroticism, student -> student.getPersonality().getNeuroticism());
    }
    private void swapStudentsBetweenGroups(List<List<Student>> groups, double globalAverage,
                                           ToDoubleFunction<Student> traitGetter) {
        List<Student> highTraitStudents = groups.stream()
                .flatMap(List::stream)
                .filter(student -> traitGetter.applyAsDouble(student) > globalAverage)
                .toList();
        List<Student> lowTraitStudents = groups.stream()
                .flatMap(List::stream)
                .filter(student -> traitGetter.applyAsDouble(student) <= globalAverage)
                .toList();
        System.out.println(highTraitStudents);
        System.out.println(lowTraitStudents);
        if (!highTraitStudents.isEmpty() && !lowTraitStudents.isEmpty()) {
            Student highTraitStudent = highTraitStudents.get(0);
            Student lowTraitStudent = lowTraitStudents.get(0);
            List<Student> highGroup = groups.stream()
                    .filter(group -> group.contains(highTraitStudent))
                    .findFirst()
                    .orElse(null);
            List<Student> lowGroup = groups.stream()
                    .filter(group -> group.contains(lowTraitStudent))
                    .findFirst()
                    .orElse(null);
            if (highGroup != null && lowGroup != null) {
                // Swap students between groups
                highGroup.remove(highTraitStudent);
                lowGroup.remove(lowTraitStudent);

                highGroup.add(lowTraitStudent);
                lowGroup.add(highTraitStudent);

            }
        }
    }
    private double calculateGlobalAverage(List<List<Student>> groups, ToDoubleFunction<Student> traitGetter) {
        return groups.stream()
                .flatMap(List::stream)
                .mapToDouble(traitGetter)
                .average()
                .orElse(0.0);
    }

    private List<Double> avgTrait(List<List<Student>> groups, ToDoubleFunction<Student> traitGetter){
        List<Double> trait = new ArrayList<>();
        List<Double> avgTrait = new ArrayList<>();
        for (List<Student> students : groups){
            for (Student student : students){
                trait.add(traitGetter.applyAsDouble(student));
            }
            avgTrait.add(avg(trait));
            trait.clear();
        }
        return avgTrait;
    }

    private List<Double> avgStudentGpa(List<List<Student>> groups){
        List<Double> cgpas = new ArrayList<>();
        List<Double> avgCgpa = new ArrayList<>();
        for (List<Student> students : groups){
            for (Student student : students){
                cgpas.add(student.getCgpa());
            }
            avgCgpa.add(avg(cgpas));
            cgpas.clear();
        }
        return avgCgpa;
    }
    private static double avg(List<Double> cgpa){
        double sum = 0;
        for (double num : cgpa){
            sum += num;
        }
//        System.out.println(sum);
        return sum/cgpa.size();
    }

    public List<Student> readStudentsFromExcel(MultipartFile file) throws IOException {
        List<Student> students = new ArrayList<>();
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
            Row row = sheet.getRow(rowIndex);
            if (row != null) {
                String name = getCellValueAsString(row.getCell(0));
                String matricNo = getCellValueAsString(row.getCell(1));
                String sex = getCellValueAsString(row.getCell(2));
                Double cgpa = getCellValueAsDouble(row.getCell(3));
                Double openness = getCellValueAsDouble(row.getCell(4));
                Double conscientiousness = getCellValueAsDouble(row.getCell(5));
                Double extraversion = getCellValueAsDouble(row.getCell(6));
                Double agreeableness = getCellValueAsDouble(row.getCell(7));
                Double neuroticism = getCellValueAsDouble(row.getCell(8));
                Personality personality = Personality.builder()
                        .openness(openness)
                        .conscientiousness(conscientiousness)
                        .extraversion(extraversion)
                        .agreeableness(agreeableness)
                        .neuroticism(neuroticism)
                        .build();
                if (name != null && matricNo != null && sex != null && cgpa!= null && personality != null)
                    students.add(createStudent(new Student(name, matricNo, sex, cgpa, personality)));
            }
            workbook.close();
        }
        return students;
    }

    private String getCellValueAsString(Cell cell) {
        return cell != null ? cell.getStringCellValue() : null;
    }
    private Double getCellValueAsDouble(Cell cell) {
        return cell != null ? (double) cell.getNumericCellValue() : null;
    }
    public List<Student> viewStudents(){
        return studentRepository.findAll();
    };
    public Student addStudent(Student student){
        var student1 = createStudent(student);
        Student savedStudent = studentRepository.save(student);
        Personality personality = student.getPersonality();
        if (personality != null) {
            personality.setStudent(savedStudent);
            Personality savedPersonality = personalityRepository.save(personality);
            savedStudent.setPersonality(savedPersonality);
        }
        return student1;
    }
    public List<Student> addStudents(List<Student> students){
        for (Student student : students){
            Student savedStudent = studentRepository.save(student);
            Personality personality = student.getPersonality();
            if (personality != null) {
                personality.setStudent(savedStudent);
                Personality savedPersonality = personalityRepository.save(personality);
                savedStudent.setPersonality(savedPersonality);
            }
        }
        return students;
    }
    private static String setGrade(double cgpa){
        if (cgpa >= 4.50 && cgpa <= 5.0){
            return "First Class";
        } else if (cgpa >= 3.50 && cgpa <= 4.49) {
            return "Second Class Upper Division";
        } else if (cgpa >= 2.40 && cgpa <= 3.49) {
            return "Second Class Lower Division";
        } else if (cgpa >= 1.50 && cgpa <= 2.39) {
            return "Third Class";
        }else
            return null;
    }
    public Student createStudent(Student student){
        return Student.builder()
                .name(student.getName())
                .matricNo(student.getMatricNo())
                .sex(student.getSex())
                .cgpa(student.getCgpa())
                .grade(setGrade(student.getCgpa()))
                .personality(student.getPersonality())
                .build();
    }
    public void deleteStudents() {
        studentRepository.deleteAll();
        personalityRepository.deleteAll();
    }
    public Optional<Student> findByMatric_no(MatricRequest matricRequest) {
        return studentRepository.findByMatricNo(matricRequest.getMatricNo());
    }

    public List<List<Student>> getBalancedGroupsFromExcel(MultipartFile file, int numberOfGroups) throws IOException {
        studentRepository.deleteAll();
        var students = readStudentsFromExcel(file);
        addStudents(students);
        return createBalancedGroups(students,numberOfGroups);
    }
    public List<List<Student>> uploadFile(MultipartFile file, int numberOfGroups) throws IOException {
        return getBalancedGroupsFromExcel(file,numberOfGroups);
    }


    public ByteArrayInputStream generateExcel(MultipartFile file, int numberOfGroups) throws IOException{
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename()));
        body.add("numberOfGroups", numberOfGroups);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body,headers);
        ResponseEntity<List<List<Student>>> response = restTemplate.exchange(
                "http://localhost:8080/fyp/upload",
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<List<List<Student>>>() {}
        );
        List<List<Student>> group = response.getBody();
        System.out.println(group);
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Students");

        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("Name");
        headerRow.createCell(1).setCellValue("Matric No");
        headerRow.createCell(2).setCellValue("Group No");

        int rowNum = 1;
        int groupNo = 0;
        for (List<Student> students : group){
            groupNo++;
            for (Student student : students){
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(student.getName());
                row.createCell(1).setCellValue(student.getMatricNo());
                row.createCell(2).setCellValue(groupNo);
            }
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);
        workbook.close();
        return new ByteArrayInputStream(out.toByteArray());
    }
    private static class MultipartInputStreamFileResource extends InputStreamResource {
        private final String filename;

        MultipartInputStreamFileResource(InputStream inputStream, String filename) {
            super(inputStream);
            this.filename = filename;
        }

        @Override
        public String getFilename() {
            return this.filename;
        }

        @Override
        public long contentLength() throws IOException {
            return -1; // We do not know the length of the stream
        }
    }
}
