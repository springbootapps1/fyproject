package com.fyp.project.repository;

import com.fyp.project.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student,Long> {
    Optional<Student> findByMatricNo(String matricNo);
}
