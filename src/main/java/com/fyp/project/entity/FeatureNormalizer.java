package com.fyp.project.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Data
public class FeatureNormalizer {
    private double minOpenness;
    private double maxOpenness;
    private double minConscientiousness;
    private double maxConscientiousness;
    private double minExtraversion;
    private double maxExtraversion;
    private double minAgreeableness;
    private double maxAgreeableness;
    private double minNeuroticism;
    private double maxNeuroticism;
    private double minGrade;
    private double maxGrade;

    public void findMinMaxValues(List<Student> students) {
        // Initialize min values to positive infinity and max values to negative infinity
        minOpenness = Double.POSITIVE_INFINITY;
        maxOpenness = Double.NEGATIVE_INFINITY;
        minConscientiousness = Double.POSITIVE_INFINITY;
        maxConscientiousness = Double.NEGATIVE_INFINITY;
        minExtraversion = Double.POSITIVE_INFINITY;
        maxExtraversion = Double.NEGATIVE_INFINITY;
        minAgreeableness = Double.POSITIVE_INFINITY;
        maxAgreeableness = Double.NEGATIVE_INFINITY;
        minNeuroticism = Double.POSITIVE_INFINITY;
        maxNeuroticism = Double.NEGATIVE_INFINITY;
        minGrade = Double.POSITIVE_INFINITY;
        maxGrade = Double.NEGATIVE_INFINITY;

        // Iterate through all students to find min and max values
        for (Student student : students) {
            double openness = student.getPersonality().getOpenness();
            double conscientiousness = student.getPersonality().getConscientiousness();
            double extraversion = student.getPersonality().getExtraversion();
            double agreeableness = student.getPersonality().getAgreeableness();
            double neuroticism = student.getPersonality().getNeuroticism();
            double cgpa = student.getCgpa();
//            System.out.println("Student data: Openness = " + openness + ", Conscientiousness = " + conscientiousness +
//                    ", Extraversion = " + extraversion + ", Agreeableness = " + agreeableness +
//                    ", Neuroticism = " + neuroticism + ", CGPA = " + cgpa);

            if (openness < minOpenness) minOpenness = openness;
            if (openness > maxOpenness) maxOpenness = openness;
            if (conscientiousness < minConscientiousness) minConscientiousness = conscientiousness;
            if (conscientiousness > maxConscientiousness) maxConscientiousness = conscientiousness;
            if (extraversion < minExtraversion) minExtraversion = extraversion;
            if (extraversion > maxExtraversion) maxExtraversion = extraversion;
            if (agreeableness < minAgreeableness) minAgreeableness = agreeableness;
            if (agreeableness > maxAgreeableness) maxAgreeableness = agreeableness;
            if (neuroticism < minNeuroticism) minNeuroticism = neuroticism;
            if (neuroticism > maxNeuroticism) maxNeuroticism = neuroticism;
            if (cgpa < minGrade) minGrade = cgpa;
            if (cgpa > maxGrade) maxGrade = cgpa;
        }
//        System.out.println("Min/Max values found:");
//        System.out.println("Openness: " + minOpenness + " / " + maxOpenness);
//        System.out.println("Conscientiousness: " + minConscientiousness + " / " + maxConscientiousness);
//        System.out.println("Extraversion: " + minExtraversion + " / " + maxExtraversion);
//        System.out.println("Agreeableness: " + minAgreeableness + " / " + maxAgreeableness);
//        System.out.println("Neuroticism: " + minNeuroticism + " / " + maxNeuroticism);
//        System.out.println("Grade: " + minGrade + " / " + maxGrade);
    }

    private double normalize(double value, double min, double max) {
        return (value - min) / (max - min);
    }

    public List<Student> normalizeStudents(List<Student> students) {
        for (Student student : students){
            normalize(student.getPersonality().getOpenness(), minOpenness, maxOpenness);
            normalize(student.getPersonality().getConscientiousness(), minConscientiousness, maxConscientiousness);
            normalize(student.getPersonality().getExtraversion(), minExtraversion, maxExtraversion);
            normalize(student.getPersonality().getAgreeableness(), minAgreeableness, maxAgreeableness);
            normalize(student.getPersonality().getNeuroticism(), minNeuroticism, maxNeuroticism);
            normalize(student.getCgpa(), minGrade, maxGrade);
        }
        return students;
    }
    public void normalizeGrade(Student student){
        normalize(student.getCgpa(), minGrade, maxGrade);
    }
}

