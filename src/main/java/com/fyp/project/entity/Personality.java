package com.fyp.project.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Personality {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long personality_id;
    private Double openness;
    private Double conscientiousness;
    private Double extraversion;
    private Double agreeableness;
    private Double neuroticism;
    @OneToOne
    @JoinColumn(name = "student_id")
    @JsonBackReference
    private Student student;




    @Override
    public String toString() {
        return "Personality{" +
                "personality_id=" + personality_id +
                ", openness=" + openness +
                ", conscientiousness=" + conscientiousness +
                ", extraversion=" + extraversion +
                ", agreeableness=" + agreeableness +
                ", neuroticism=" + neuroticism +
                '}';
    }
}
