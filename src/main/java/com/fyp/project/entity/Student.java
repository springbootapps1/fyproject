package com.fyp.project.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Student {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "student_id_sequence")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long student_id;
    private String name;
    private String matricNo;
    private String sex;
    private Double cgpa;
    private String grade;
    @OneToOne(mappedBy = "student", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Personality personality;




    public Student(String name, String matricNo, String sex, Double cgpa, Personality personality) {
        this.name = name;
        this.matricNo = matricNo;
        this.sex = sex;
        this.cgpa = cgpa;
        this.personality = personality;
    }

    @Override
    public String toString() {
        return "Student{" +
                "student_id=" + student_id +
                ", name='" + name + '\'' +
                ", matricNo='" + matricNo + '\'' +
                ", sex='" + sex + '\'' +
                ", cgpa=" + cgpa +
                ", grade='" + grade + '\'' +
                '}';
    }
}
