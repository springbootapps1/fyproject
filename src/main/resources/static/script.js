document.getElementById('uploadForm').addEventListener('submit', async function(event) {
    event.preventDefault(); // Prevent the default form submission

    // Get the file and number of groups input elements
    const fileInput = document.getElementById('file');
    const numberOfGroupsInput = document.getElementById('numberOfGroups');
    const messageDiv = document.getElementById('message');
    
    // Clear previous messages
    messageDiv.textContent = '';
    
  
    const formData = new FormData();
    formData.append('file', fileInput.files[0]);
    formData.append('numberOfGroups', numberOfGroupsInput.value);
    try {
        const response = await fetch('http://localhost:8080/fyp/download', {
            method: 'POST',
            body: formData
        });
        if (response.ok) {
            const blob = await response.blob();
            const url = window.URL.createObjectURL(blob);

            const a = document.createElement('a');
            a.href = url;
            a.download = 'groups.xlsx';
            document.body.appendChild(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url); 
            messageDiv.textContent = 'Groups file downloaded successfully.';
            messageDiv.style.color = 'green';
        } else {
            const errorMessage = await response.text();
            messageDiv.textContent = 'Failed to create groups: ' + errorMessage;
        }
    } catch (error) {
        messageDiv.textContent = 'An error occurred: ' + error.message;
    }
});